// delete
var setDelete = function(element){
    element.addEventListener('click', function() {
      makeRequest(this.getAttribute('data-id'), 'DELETE', null, function(res) {
         element.closest('aside').remove();
      });
    })
};
document.querySelectorAll('.delete').forEach(setDelete);

// INSERT

document.getElementById('addGare').addEventListener('submit', function(evt) {
  evt.preventDefault();
  var form = evt.target;
  var datas = {};
  form.querySelectorAll('[name]').forEach(function(el){
    datas[el.getAttribute('name')] = el.value
  });
  makeRequest('', form.getAttribute('method'), datas, function(res){
    var span = document.createElement("span");
    span.innerHTML = res.response;
    var buttonDelete = span.querySelector(".delete");
    var buttonUpdate = span.querySelector(".update");
    setDelete(buttonDelete);
    setUpdate(buttonUpdate);
    var container = document.querySelector('.gares-container');
    container.appendChild(span.firstChild);
  })
});



// // UPDATE
// var setUpdate = function(element){
//   element.addEventListener('click', function() {
//     var name = this.getAttribute('data-state') == "false";
//     makeRequest(this.getAttribute('data-id'), 'PUT', {nom : name}, function(res){
//         var span = document.createElement("span");
//         span.innerHTML = res.response;
//         var buttonDelete = span.querySelector(".delete");
//         var buttonUpdate = span.querySelector(".ok");
//         setDelete(buttonDelete);
//         setUpdate(buttonUpdate);
//         element.closest('aside').replaceWith(span.firstChild);
//     });
//   })
// }
// document.querySelectorAll('.update').forEach(setUpdate);






function makeRequest(id, method, datas, callback) {
    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function(){
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
          if (httpRequest.status === 200) {
            callback(JSON.parse(httpRequest.responseText));
          } else {
            alert('There was a problem with the request.');
          }
        }
    };
    httpRequest.open(method, '/' + id);
    httpRequest.setRequestHeader('Content-Type', 'application/json');
    httpRequest.send(datas ? JSON.stringify(datas) : null);
  }
