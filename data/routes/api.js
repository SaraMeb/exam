var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectId
var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/examD2D13', { useNewUrlParser: true }, function (err, client) {
  if (err) throw err
  let db = client.db('examD2D13');

  router.use(function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    next()
  });


  router.get('/', function(req, res, next) {
    db.collection('gares').find({}).toArray(function(error, datas) {
    return res.json(datas) ;

    })
  });

  //ajouter une gare:

  router.post('/', function(req, res, next) {
    db.collection('gares').insertOne({nom : req.body.name, datecreation: new Date(), siret: req.body.siret, theme: 'DEPLACEMENT', idexterne : req.body.idexterne , gid: req.body.gid, identifiant :req.body.identifiant}, function(error, result) {
      if(err) return next(err);
      db.collection('gares').findOne({_id: result.insertedId}, function(err, doc) {
        res.render('gares', {gare : doc}, function(err, html) {
          return res.json({
            response : html
          })
        })
      })
    })
  })
  

//Supprimer une gare:
router.delete('/:id', function(req, res, next) {
    gareId = req.params.id,
      db.collection('gares').deleteOne({_id : ObjectId(gareId)}, function(error, result) {
        if(err) return next(err);
        return res.json(result) ;
      })
    });












});
module.exports = router;
